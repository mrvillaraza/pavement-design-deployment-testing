import React, { useState, useEffect } from 'react';
import { Form, Container, Col, Row, Button } from 'react-bootstrap';
import Head from 'next/head';

/*import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faQuestionCircle } from '@fortawesome/free-solid-svg-icons';

// importing illustrations
import concstrength from '../public/concrete_strength.jpg';
import pavebuildup from '../public/pavement buildup.jpg';
import soilclass from '../public/soil classifications.jpg';
import typicalyoungs from '../public/typical-youngs-modulus.jpg'; */

export default function Home() {
  
    const [subbaseType, setSubbaseType] = useState('');

    const [subgradeParam, setSubgradeParam] = useState('');
    const [subgradeClassInput, setSubgradeClassInput] = useState('');
    const [subgradeCBRInput, setSubgradeCBRInput] = useState('');
    const [subgradeClass, setSubgradeClass] = useState('none');
    const [cbr, setCbr] = useState();

    const [designSubbaseThickness, setDesignSubbaseThickness] = useState(0);

    const [designConcreteMethod, setDesignConcreteMethod] = useState('');
    const [designEmpiricalInput, setDesignEmpiricalInput] = useState('');
    const [designConcreteUsage, setDesignConcreteUsage] = useState('');

    const [designRigorousInput, setDesignRigorousInput] = useState('');
    const [loadingType, setLoadingType] = useState('wheel')
    const [wheelSpacing, setWheelSpacing] = useState(0);
    const [youngsE, setYoungsE] = useState(0);
    const [soilLayerDepth, setSoilLayerDepth] = useState(0);
    const [axleLoad, setAxleLoad] = useState(0);
    const [concreteStrength, setConcreteStrength] = useState(20);
    const [designLife, setDesignLife] = useState(0);
    const [dailyRep, setDailyRep] = useState(0);

    const [ illustration, setIllustration ] = useState('pavement buildup');
    const [ figure, setFigure ] = useState('');
    const [ results, setResults ] = useState('');

    const [isCorrect, SetIsCorrect] = useState(false);

/////////////////////////////////////////////////////////////////////////////

   /*  // update and render the relevant illustration
    useEffect( () => {
        
        if (illustration === 'pavement buildup') {
            setFigure(
                <img
                    src={pavebuildup}
                    alt=''
                    id='paveBuildup'
                />
            )
        }

        if (illustration === 'concrete strength') {
            setFigure(
                <img
                    src={concstrength}
                    alt=''
                    id='concStrength'
                />
            )
        }

        if (illustration === 'soil classifications') {
            setFigure(
                <img
                    src={soilclass}
                    alt=''
                    id='soilClass'
                />
            )
        }

        if (illustration === 'youngs modulus') {
            setFigure(
                <img
                    src={typicalyoungs}
                    alt=''
                    id='typicalYoungs'
                />
            )

        }

    }, [illustration]) */

/////////////////////////////////////////////////////////////////////////////

    // check whether all inputs are correct

    useEffect( () => {

        // assume incorrect until proven correct
        SetIsCorrect(false)

        if (subgradeParam !== '') {

            if ((subgradeClass !== 'none' || cbr >= 0) && cbr !== '' && subbaseType !== '' && designConcreteMethod !== '') {

                if ((designConcreteMethod === 'Simplified' && (designConcreteUsage === 'usage1' || designConcreteUsage === 'usage2'))) {
                    SetIsCorrect(true)

                }

                if ((designConcreteMethod === 'Simplified' && designConcreteUsage === 'usage1')) {
                    
                    setConcreteStrength(32)
                    SetIsCorrect(true)

                }

                if ((designConcreteMethod === 'Simplified' && designConcreteUsage === 'usage2')) {
                    
                    setConcreteStrength(40)
                    SetIsCorrect(true)

                }

                if ((designConcreteMethod === 'Rigorous' &&
                    designLife > 0 && designLife !== '' &&
                    dailyRep > 0 && dailyRep !== '' &&
                    youngsE > 0 && youngsE !== '' &&
                    soilLayerDepth > 0 && soilLayerDepth !== '' &&
                    axleLoad > 0 && axleLoad !== '' &&
                    wheelSpacing > 0 && wheelSpacing !== '')) {
    
                    SetIsCorrect(true)

                }

            }

        }

    }, [subgradeParam, subgradeClass, cbr, subbaseType, designConcreteMethod, designConcreteUsage, designLife, dailyRep, youngsE, soilLayerDepth, axleLoad, wheelSpacing])

/////////////////////////////////////////////////////////////////////////////

    // Output relevant input fields depending on the "Subgrade Parameter" choices made
    useEffect( () => {
        
        console.log(subgradeParam)

        if (subgradeParam === 'subgradeClass') {
            setSubgradeCBRInput('')
            setCbr()

            setSubgradeClassInput(
                <Form.Group className='formChoicesSubgrade'>
                     <Form.Check
                        className='mx-1'
                        type='radio'
                        label='Poor'
                        name='subgradeClassChoices'
                        value='poor'
                        onChange= {(e) => setSubgradeClass(e.target.value)}
                    />

                    <Form.Check
                        className='mx-1'
                        type='radio'
                        label='Medium'
                        name='subgradeClassChoices'
                        value='medium'
                        onChange= {(e) => setSubgradeClass(e.target.value)}
                    />

                    <Form.Check
                        className='mx-1'
                        type='radio'
                        label='Good'
                        name='subgradeClassChoices'
                        value='good'
                        onChange= {(e) => setSubgradeClass(e.target.value)}
                    />
                </Form.Group>
            )
        }
        
        if (subgradeParam === 'cbrValue') {
            setSubgradeClassInput('')
            setSubgradeClass('none')

            setSubgradeCBRInput(
                <Form.Group>
                    <Col md='6'>
                        <Form.Control 
                            type="number" 
                            placeholder="Enter CBR value" 
                            value={cbr}
                            onChange={(e) => setCbr(e.target.value)}
                            required
                        />
                    </Col>
                </Form.Group>
            )

            console.log(cbr)
        }

    }, [subgradeParam, subgradeClass, cbr])

/////////////////////////////////////////////////////////////////////////////

    // Output relevant input fields depending on "Concrete Slab Design Method" choice
    useEffect( () => {

        if (designConcreteMethod === 'Simplified') {

            setWheelSpacing(0)
            setYoungsE(0)
            setSoilLayerDepth(0)
            setAxleLoad(0)
            setDesignLife(0)
            setDailyRep(0)
            
            setDesignRigorousInput('')

            setDesignEmpiricalInput(
                <Form.Group className='formChoicesSimplified'>
                    <Form.Check
                        className='mx-1'
                        type='radio'
                        label='Shops, garages mainly for private cars, light industrial premises with live loading up to 5kPa'
                        name='usage'
                        value='usage1'
                        onChange= {(e) => setDesignConcreteUsage(e.target.value)}
                    />

                    <Form.Check
                        className='mx-1'
                        type='radio'
                        label='Garages mainly for commercial vehicles, industrial premises, waterhouses, with live loading between 5 and 20 kPa'
                        name='usage'
                        value='usage2'
                        onChange= {(e) => setDesignConcreteUsage(e.target.value)}
                    />

                </Form.Group>
            )
        }

        if (designConcreteMethod === 'Rigorous') {
            
            setDesignEmpiricalInput('')

            setDesignRigorousInput(
               <Form.Group className='formChoicesRigorous py-2'>

                    <Row className='mx-1 py-1'>
                        <Form.Label column sm md='7'>Loading Type :</Form.Label>
                        <Col sm md='5'>
                            <Form.Control
                                as='select'
                                onChange={(e) => setLoadingType(e.target.value)}
                                required
                            >
                                <option value='wheel'>Wheel</option>
                                <option value='post' disabled>Post</option>
                                <option value='distributed' disabled>Distributed</option>
                            </Form.Control>
                        </Col>
                    </Row>

                    <Row className='mx-1 py-1'>
                        <Form.Label column sm md='7'>Concrete Strength, f'c :</Form.Label>
                        <Col sm md='5'>
                            <Form.Control
                                as='select'
                                value={concreteStrength}
                                onChange={(e) => setConcreteStrength(e.target.value)}
                                required
                            >
                                <option value='20'>20 MPa</option>
                                <option value='25'>25 MPa</option>
                                <option value='32'>32 MPa</option>
                                <option value='40'>40 MPa</option>
                                <option value='50'>50 MPa</option>
                            </Form.Control>
                        </Col>
                    </Row>

                    <Row className='mx-1 py-1'>
                        <Form.Label column sm md='7'>Design Life (year) :</Form.Label>
                        <Col sm md='5'>
                            <Form.Control 
                                type="number" 
                                value={designLife}
                                onChange={(e) => setDesignLife(e.target.value)}
                                required
                            />
                        </Col>
                    </Row>

                    <Row className='mx-1 py-1'>
                        <Form.Label column sm md='7'>Daily Load Repetition :</Form.Label>
                        <Col sm md='5'>
                            <Form.Control 
                                type="number" 
                                value={dailyRep}
                                onChange={(e) => setDailyRep(e.target.value)}
                                required
                            />
                        </Col>
                    </Row>

                    <Row className='mx-1 py-1'>
                        <Col sm md = '8'>
                            <Form.Label>Youngs Modulus, E<sub>ss</sub> (MPa)</Form.Label>
                        </Col>
                        
                        <Col sm md='4'>
                            <Form.Control 
                                type="number" 
                                value={youngsE}
                                onChange={(e) => setYoungsE(e.target.value)}
                                required
                            />
                        </Col>

                    </Row>

                    <Row className='mx-1 py-1'>
                        <Form.Label column sm md='7'>Depth of Soil Layers (m) :</Form.Label>
                        <Col sm md='5'>
                            <Form.Control 
                                type="number" 
                                value={soilLayerDepth}
                                onChange={(e) => setSoilLayerDepth(e.target.value)}
                                required
                            />
                        </Col>
                    </Row>

                    <Row className='mx-1 py-1'>
                        <Form.Label column sm md='7'>Axle Load (kN) :</Form.Label>
                        <Col sm md='5'>
                            <Form.Control 
                                type="number" 
                                value={axleLoad}
                                onChange={(e) => setAxleLoad(e.target.value)}
                                required
                            />
                        </Col>
                    </Row>

                    <Row className='mx-1 py-1'>
                        <Form.Label column sm md='7'>Wheel Spacing (m) :</Form.Label>
                        <Col sm md='5'>
                            <Form.Control 
                                type="number" 
                                value={wheelSpacing}
                                onChange={(e) => setWheelSpacing(e.target.value)}
                                required
                            />
                        </Col>
                    </Row>                  

                </Form.Group>

            )
        }

    }, [designConcreteMethod, loadingType, concreteStrength, designLife, dailyRep, youngsE, soilLayerDepth, axleLoad, wheelSpacing])

/////////////////////////////////////////////////////////////////////////////

    // Subbase design and CBR-Subgrade Classification Conversion

    useEffect( () => {

        if (subgradeClass === 'poor') {
            console.log('Poor subgrade classification')
            setDesignSubbaseThickness(200)
        }

        if (subgradeClass === 'medium') {
            console.log('Medium subgrade classification')
            setDesignSubbaseThickness(150)
        }

        if (subgradeClass === 'good') {
            console.log('Good subgrade classification')
            setDesignSubbaseThickness(100)
        }

        if (cbr <= 0) {
            console.log('CBR cannot be negative value nor zero')
        }

        if ((cbr > 0 && cbr <= 2)) {
            console.log('Poor subgrade classification based on CBR value')
            setSubgradeClass('poor')
        }

        if ((cbr > 2 && cbr <= 10)) {
            console.log('Medium subgrade classification based on CBR value')
            setSubgradeClass('medium')
        }

        if (cbr > 10) {
            console.log('Good subgrade classification based on CBR value')
            setSubgradeClass('good')
        }

    }, [subgradeClass, cbr])

/////////////////////////////////////////////////////////////////////////////

    // Design scripts

    function design(e) {
        e.preventDefault();

        let designConcreteThickness = 0;

        if (designConcreteMethod === 'Simplified') {
            
            if (designConcreteUsage === 'usage1') {

                if ((subgradeClass === 'poor' || cbr <= 2 )) {
                    designConcreteThickness = 150
                }

                if ((subgradeClass === 'medium' || subgradeClass === 'good' || cbr > 2 )) {
                    designConcreteThickness = 130
                }

            }

            if (designConcreteUsage === 'usage2') {

                if ((subgradeClass === 'poor' || cbr <= 2 )) {
                    designConcreteThickness = 200
                }

                if ((subgradeClass === 'medium' || subgradeClass === 'good' || cbr > 2 )) {
                    designConcreteThickness = 180
                }

            }

        }

        // defining the variables outside the rigorous design code block 
        let fInternal = 0;
        let thicknessInternal = 0;
        let fE1 = 0;
        let fH1 = 0;
        let fS1 = 0;

        let fEdge = 0;
        let thicknessEdge = 0;
        let fE2 = 0;
        let fH2 = 0;
        let fS2 = 0;

        let fAll = 0;
        let k1 = 0;

        // loadRep = designLife in years * 52 weeks/year * 5 days/week * dailyRep
        let loadRep = designLife*52*5*dailyRep;
        let k2 = 1.1086*Math.pow(loadRep, -0.059)

        let k3Internal = 1.2;
        let k3Edge = 1.05;

        let k4 = 0.623*Math.pow(concreteStrength, 0.1677);

        if (designConcreteMethod === 'Rigorous') {

            if (loadingType === 'wheel') {
                // perform design for wheel loading
                k1 = 0.90

                 // design tensile strength of concrete
                fAll = k1*k2*0.7*Math.pow(concreteStrength, 0.5);

                // factor for short term young's modulus, Ess
                if ((youngsE > 0 && youngsE < 50)) {
                    fE1 = 2*Math.pow(10, -6)*Math.pow(youngsE, 3) - 0.0004*Math.pow(youngsE, 2) + 0.0265*youngsE + 0.7446
                    
                    fE2 = 6*Math.pow(10, -6)*Math.pow(youngsE, 3) - 0.0007*Math.pow(youngsE, 2) + 0.0368*youngsE + 0.6977
                }
                if ((youngsE >= 50 && youngsE < 100)) {
                    fE1 = Math.pow(10, -6)*Math.pow(youngsE, 3) - 0.0002*Math.pow(youngsE, 2) + 0.0096*youngsE + 1.2786

                    fE2 = 4*Math.pow(10, -6)*Math.pow(youngsE, 3) - 0.0008*Math.pow(youngsE, 2) + 0.0602*youngsE + 0.0246
                }
                if ((youngsE >= 100 && youngsE < 150)) {
                    fE1 = 7*Math.pow(10, -8)*Math.pow(youngsE, 3) - 0.00008*Math.pow(youngsE, 2) + 0.02*youngsE + 0.2864

                    fE2 = Math.pow(10, -6)*Math.pow(youngsE, 3) - 0.0001*Math.pow(youngsE, 2) + 0.0827*youngsE - 2.3342
                }
                if (youngsE > 150) {
                    // unrealistic Ess
                    fE1 = 999
                    fE2 = 999
                }

                // factor for depth of equivalent uniform layer of soil, H
                fH1 = 7*Math.pow(10, -5)*Math.pow(soilLayerDepth, 4) - 0.0026*Math.pow(soilLayerDepth, 3) + 0.0345*Math.pow(soilLayerDepth, 2) - 0.1897*soilLayerDepth + 1.3537

                if ((soilLayerDepth > 0 && soilLayerDepth < 0.5)) {
                    fH2 = -0.1*soilLayerDepth + 1.35
                }

                if ((soilLayerDepth <= 0.5 && soilLayerDepth < 6)) {                
                    fH2 = 0.0014*Math.pow(soilLayerDepth, 4) - 0.0248*Math.pow(soilLayerDepth, 3) + 0.161*Math.pow(soilLayerDepth, 2) - 0.4584*soilLayerDepth + 1.4983
                }

                if (soilLayerDepth >= 6) {
                    fH2 = 0.98
                }

                // factor for center to center wheel spacing
                fS1 = -0.0433*Math.pow(wheelSpacing, 2) + 0.3047*wheelSpacing + 0.6401

                fS2 = -0.0068*Math.pow(wheelSpacing, 4) + 0.0675*Math.pow(wheelSpacing, 3) - 0.2765*Math.pow(wheelSpacing, 2) + 0.6068*wheelSpacing + 0.5244

                // design stress factor, F1 (both edge and internal loading)
                fInternal = fAll*fE1*fH1*fS1*k3Internal*k4
                fEdge = fAll*fE2*fH2*fS2*k3Edge*k4

                // compute for thickness based on internal and edge wheel axle loading
                if (axleLoad <= 50) {
                    thicknessInternal = 231.92*Math.pow(fInternal, -0.626)

                    thicknessEdge = 327.91*Math.pow(fEdge, -0.619)

                    designConcreteThickness = Math.max(thicknessInternal, thicknessEdge)
                    
                }
                if ((axleLoad > 50 && axleLoad <= 80)) {
                    thicknessInternal = 294.24*Math.pow(fInternal, -0.613)

                    thicknessEdge = 429.28*Math.pow(fEdge, -0.624)

                    designConcreteThickness = Math.max(thicknessInternal, thicknessEdge)
                }
                if ((axleLoad > 80 && axleLoad <= 100)) {
                    thicknessInternal = 360.30*Math.pow(fInternal, -0.61)

                    thicknessEdge = 555.02*Math.pow(fEdge, -0.67)

                    designConcreteThickness = Math.max(thicknessInternal, thicknessEdge)
                }
                if ((axleLoad > 100 && axleLoad <= 200)) {
                    thicknessInternal = 540.30*Math.pow(fInternal, -0.65)

                    thicknessEdge = 786.95*Math.pow(fEdge, -0.644)

                    designConcreteThickness = Math.max(thicknessInternal, thicknessEdge)
                }
                if ((axleLoad > 200 && axleLoad <= 400)) {
                    thicknessInternal = 792.75*Math.pow(fInternal, -0.658)

                    thicknessEdge = 1268.9*Math.pow(fEdge, -0.71)

                    designConcreteThickness = Math.max(thicknessInternal, thicknessEdge)
                }
                if ((axleLoad > 400 && axleLoad <= 600)) {
                    thicknessInternal = 993.19*Math.pow(fInternal, -0.641)

                    thicknessEdge = 1379.5*Math.pow(fEdge, -0.624)

                    designConcreteThickness = Math.max(thicknessInternal, thicknessEdge)
                }
                if ((axleLoad > 600 && axleLoad <= 800)) {
                    thicknessInternal = 1159.60*Math.pow(fInternal, -0.63)

                    thicknessEdge = 999

                    designConcreteThickness = Math.max(thicknessInternal, thicknessEdge)
                }
                if (axleLoad > 800) {
                    thicknessInternal = 999

                    thicknessEdge = 999

                    designConcreteThickness = Math.max(thicknessInternal, thicknessEdge)
                }

                // Console log the computations
                console.log(`fAll is ` + fAll)
                console.log(`fE1 is ` + fE1)
                console.log(`fE2 is ` + fE2)
                console.log(`fS1 is ` + fS1)
                console.log(`fS2 is ` + fS2)
                console.log(`fH1 is ` + fH1)
                console.log(`fH2 is ` + fH2)
                console.log(`fInternal is ` + fInternal)
                console.log(`fEdge is ` + fEdge)
                console.log(`thicknessInternal is ` + thicknessInternal)
                console.log(`thicknessEdge is ` + thicknessEdge)

            }

        }

        setIllustration('pavement buildup')

        console.log(concreteStrength);
        console.log(designConcreteThickness);
        console.log(thicknessInternal);
        console.log(thicknessEdge);

        setResults(
            <React.Fragment>
                <div className='resultsBorderBox p-3'>

                    <h4 className='pb-2'><ins>{designConcreteMethod}</ins> Concrete Slab Design Results:</h4>

                    {(designConcreteMethod === 'Simplified')
                        ?
                            <>
                                <h5 className='mx-3'>Concrete Strength = {concreteStrength} MPa</h5>
                                <h5 className='mx-3'>Concrete Thickness = {designConcreteThickness} mm</h5>
                            </>
                        :
                            <>
                                <h5 className='mx-3'>Concrete Strength = {concreteStrength} MPa</h5>
                                <h5 className='mx-3'>Concrete Thickness = {Math.ceil(designConcreteThickness/10)*10} mm</h5>
                                <li className='mx-5'>Required Concrete Thickness for Internal Loading = {thicknessInternal.toFixed(2)} mm</li>
                                <li className='mx-5'>Required Concrete Thickness for Edge Loading = {thicknessEdge.toFixed(2)} mm</li>
                            </>
                    }

                    <h5 className='mx-3 pt-1'>Subbase Thickness = {designSubbaseThickness} mm</h5>

                </div>
            </React.Fragment>
        )

    }

/////////////////////////////////////////////////////////////////////////////

    return (
        <React.Fragment>
            
            <Container>

                <Row className='justify-content-center pt-3'>
                    <h2>Concrete Industrial Floor and Pavement Design</h2>
                </Row>
                
                <Row className='justify-content-center pb-3'>
                    <h5>Cement and Concrete Association of Australia</h5>
                </Row>

                <Row className='userInputs'>
                    <Col xs = '12' md lg = '5'>

                        <Form>
                            
                            <div className='pb-3'>
                                <h5 className='withIcon'>Pavement Profile</h5>

                                
                            </div>

                            <Form.Group>
                                <h5>Subgrade Parameter</h5>
                                
                                <div>
                                    
                                    <Form.Check
                                        type='radio'
                                        className='withIcon'
                                        label='Subgrade Classification'
                                        name='subgrParam'
                                        value='subgradeClass'
                                        onChange= {(e) => setSubgradeParam(e.target.value)}

                                    />

                                    

                                </div>

                                <div className='mx-4 my-2'>
                                    {subgradeClassInput}
                                </div>
                                
                                <Form.Check
                                    type='radio'
                                    label='CBR Value'
                                    name='subgrParam'
                                    value='cbrValue'
                                    onChange= {(e) => setSubgradeParam(e.target.value)}
                                />

                                {subgradeCBRInput}

                            </Form.Group>

                            <Form.Group>
                                <h5>Subbase Type (No Bearing*)</h5>

                                <Form.Check
                                    className='pb-1'
                                    type='radio'
                                    label='Unbound Granular Subbase'
                                    name='subbaseType'
                                    value='unbound'
                                    onChange= {(e) => setSubbaseType(e.target.value)}
                                />

                                <Form.Check
                                    className='pb-1'
                                    type='radio'
                                    label='Bound Subbase'
                                    name='subbaseType'
                                    value='bound'
                                    onChange= {(e) => setSubbaseType(e.target.value)}
                                />

                            </Form.Group>

                            <Form.Group>
                                <h5>Concrete Slab Design Method</h5>
                                
                                <Form.Check
                                    type='radio'
                                    label='Simplified Thickness Design for Lightly-Loaded Floors'
                                    name='designConcreteMethod'
                                    value='Simplified'
                                    onChange= {(e) => setDesignConcreteMethod(e.target.value)}
                                />

                                <div className='mx-4 my-2'>
                                    {designEmpiricalInput}
                                </div>

                                <Form.Check
                                    type='radio'
                                    label='Rigorous Thickness Design for Dynamic Load'
                                    name='designConcreteMethod'
                                    value='Rigorous'
                                    onChange= {(e) => setDesignConcreteMethod(e.target.value)}
                                />

                                <div className='mx-4 my-2'>
                                    {designRigorousInput}
                                </div>

                            </Form.Group>

                        </Form>

                        {
                            (isCorrect === true)
                                ?
                                    <Button className='bg-primary' onClick={(e) => design(e)}>Design</Button>
                                :
                                    <Button className='bg-danger' disabled>Design</Button>
                        }

                    </Col>

                    <Col xs = '12' md lg ='7'>
                        
                        <div className='imageContainer pb-3'>
                            {figure}
                        </div>

                        {results}
                        
                    </Col>

                </Row>

            </Container>
            
        </React.Fragment>

    )

}
